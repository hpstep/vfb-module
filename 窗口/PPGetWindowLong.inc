Function PPGetWindowLong(ByVal hWnd As hWnd ,ByVal nIndex As Long) As Long'跨进程获取窗口回调函数地址
   Dim TargetProcessId As ULong
   Dim MyProcessId     As ULong
   GetWindowThreadProcessId(hWnd ,@TargetProcessId)
   Asm
      mov eax           ,DWORD Ptr fs : [&H18]
      mov ecx           ,[eax + &H20]
      mov [MyProcessId] ,ecx
      mov ecx           ,[TargetProcessId]
      mov [eax + &H20] ,ecx
   End Asm
   If IsWindowUnicode(hWnd) = CTRUE Then
      GetWindowLongW(hWnd ,GWL_WNDPROC)
   Else
      GetWindowLongA(hWnd ,GWL_WNDPROC)
   End If
   Asm
      mov [Function] ,eax
      push [MyProcessId]
      mov ecx ,DWORD Ptr fs : [&H18]
      pop [ecx + &H20]
   End Asm
End Function