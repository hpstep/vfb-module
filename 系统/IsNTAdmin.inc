Function IsNTAdmin() As BOOL '检查用户是否有管理员权限。
   '返回 True = 有管理员权限  , False = 没有管理员权限
   Dim SidAuthority As SID_IDENTIFIER_AUTHORITY = SECURITY_NT_AUTHORITY
   Dim pTokenGroups As PTOKEN_GROUPS
   Dim bSidFound As BOOL = FALSE
   Dim dwSize As DWORD
   Dim hToken As HANDLE
   Dim pSid As PSID
   
   If OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, @HToken) = 0 Then
      Return FALSE
   End If

   If GetTokenInformation(hToken, TokenGroups, NULL, 0, @dwSize) = 0 Then
      If GetLastError() <> ERROR_INSUFFICIENT_BUFFER Then
         CloseHandle(hToken)
         Return FALSE
      End If
   End If
   
   pTokenGroups = HeapAlloc(GetProcessHeap(), 0, dwSize)
   If pTokenGroups = 0 Then
      CloseHandle(hToken)
      Return FALSE
   End If
   
   If GetTokenInformation(hToken, TokenGroups, pTokenGroups, dwSize, @dwSize) = 0 Then
      HeapFree(GetProcessHeap(), 0, pTokenGroups)
      CloseHandle(hToken)
      Return FALSE
   End If
   
   CloseHandle(hToken)
   
   If AllocateAndInitializeSid(@SidAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, @pSid) = 0 Then
      HeapFree(GetProcessHeap(), 0, pTokenGroups)
      Return FALSE
   End If
   
   For I As Long = 0 To pTokenGroups->GroupCount - 1
      If EqualSid(pSid, pTokenGroups->Groups(I).Sid) <> 0 Then
         bSidFound = True
         Exit For
      End If
   Next I
   
   HeapFree(GetProcessHeap(), 0, pTokenGroups)
   FreeSid(pSid)
   
   Return bSidFound
End Function